file=paste("./data/Poisson/","parametros_llegadas", sep="")
file=paste(file,".csv",sep="")
parametros_llegadas=read.table(file,sep=",",header = TRUE,na.strings=c("*","N/D"))
parametros_llegadas=parametros_llegadas[,-1]
file=paste("./data/Poisson/","parametros_salidas", sep="")
file=paste(file,".csv",sep="")
parametros_salidas=read.table(file,sep=",",header = TRUE,na.strings=c("*","N/D"))
parametros_salidas=parametros_salidas[,-1]



duracion=15
p=1
h=1
dir.create("./data/penalizacion")
indice_estacion=0
for (estacion in Polanco)
{
  indice_estacion=indice_estacion+1
  cap=capacidad[which(capacidad[,1]==estacion),2]
  f=matrix(0,nrow = (cap+1), ncol = 2)
  for (periodo in 1:96)
  {
      file=paste("./data/estados/",estacion, sep="")
      file=paste(file,estacion,sep="/")
      file=paste(file,periodo,sep="_")
      file=paste(file,".csv",sep="")
      transicion=read.table(file,sep=",",header = TRUE,na.strings=c("*","N/D"))
      transicion=transicion[,-1]
      transicion=as.matrix(transicion)
  
      for(estado in 0:cap)
      {
        f[(estado+1),1]=estado
        f[(estado+1),2]=f[(estado+1),2]+duracion*((transicion[(estado+1),1]*parametros_llegadas[periodo,indice_estacion]*p)+
                                    transicion[(estado+1),(cap+1)]*h*parametros_salidas[periodo,indice_estacion])
      }
  }
  name=paste("./data/penalizacion/",estacion, sep="")
  name=paste(name,".csv",sep="")
  write.csv(f,name)
}