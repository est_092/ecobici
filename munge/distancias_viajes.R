library(dplyr)
library(lubridate)
library(tidyr)


load("data/base_full.RData")

dat_full=dat_full %>%
  mutate(duracion=difftime(Hora_Arribo,Hora_Retiro, units="secs")) %>%
  filter(duracion>=0 & duracion<=86400)

distancias=dat_full%>%
  group_by(Ciclo_Estacion_Arribo, Ciclo_Estacion_Retiro) %>%
  summarise(duracion=mean(duracion))  %>%
  ungroup()



matriz=distancias %>%
  spread()

estaciones=c(196,198,199,200,201,202,203,205,206,207,208,209,210,211,212,213,214,215,216,217,218
          ,219,220,221,222,223,224,225,226,228,229,230,231,233,235,236,239,240,241,451)
base=distancias

calcula_distancias=function(base, estaciones){
  dat=base %>%
    filter(Ciclo_Estacion_Arribo %in% estaciones & Ciclo_Estacion_Retiro %in% estaciones) %>%
    mutate(id=Ciclo_Estacion_Arribo^2+Ciclo_Estacion_Retiro^2) %>%
    group_by(id) %>%
    summarise(Ciclo_Estacion_Arribo=first(Ciclo_Estacion_Arribo),
              Ciclo_Estacion_Retiro=first(Ciclo_Estacion_Retiro),
              duracion=mean(duracion,na.rm = TRUE)) %>%
    ungroup() %>%
    select(-id)
  ##Nos aseguramos de tener todas las combinaciones entre estaciones
  combinaciones= dat%>%
    select(Ciclo_Estacion_Arribo, Ciclo_Estacion_Retiro) %>%
    mutate(combo=paste0(Ciclo_Estacion_Arribo,"--", Ciclo_Estacion_Retiro)) %>%
    select(combo) %>%
    .$combo
    
  ##Posibles estaciones
  estaciones_temp= expand.grid(estaciones,estaciones) %>%
    mutate(combo=paste0(Var1,"--",Var2))  %>%
    select(combo) %>%
    .$combo
    
  if(any(!estaciones_temp %in% combinaciones)){
    combinaciones=estaciones_temp[which(!estaciones_temp %in% combinaciones)] %>%
      data.frame() %>%
      separate(1,into=c("Ciclo_Estacion_Arribo","Ciclo_Estacion_Retiro"), sep="--") %>%
      mutate(duracion=NA,
             Ciclo_Estacion_Arribo=as.numeric(Ciclo_Estacion_Arribo),
             Ciclo_Estacion_Retiro=as.numeric(Ciclo_Estacion_Retiro))
    dat=dat %>%
      bind_rows(combinaciones)
  }
  dat=dat %>%
    arrange(Ciclo_Estacion_Retiro) %>%
    spread(Ciclo_Estacion_Retiro, duracion) 
  rownames(dat)=dat$Ciclo_Estacion_Arribo
  dat=dat %>%
    select(-Ciclo_Estacion_Arribo) %>%
    as.matrix()

  dat[lower.tri(dat)] <- t(dat)[lower.tri(dat)]
  return(dat)
}
