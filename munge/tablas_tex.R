files=list.files("cache/acumulado_escenarios")

files[grepl("n-40", files) & grepl("T-16200.RDS",files)] %>%
  map_dfr(function(x){
    temp=readRDS(paste0("cache/acumulado_escenarios/",x))%>% 
      mutate(mejora_total=penalizacion_inicial-penalizacion_final,mejora=1-(penalizacion_final/penalizacion_inicial))
    
  }) %>%
  filter(vehicle_capacity>15) %>%
  select(mejora_total, vehicle_capacity, alpha) %>%
  spread(alpha, mejora_total) %>%
  xtable( type = "latex") %>%
  print( file = "output/filename2.tex")


caso_base=readRDS("cache/caso_base.RDS")

files[grepl("n-40", files) & grepl("k-30",files) & (!grepl("linearizacion",files))] %>%
  # head(1)->x
  map_dfr(function(x){
    temp=readRDS(paste0("cache/resumen_escenarios/",x))%>% 
      mutate(mejora=1-(penalizacion_final/penalizacion_inicial),
             text=x ) %>%
      separate(text, into=c("N","k","alpha","T","lin","h","p"), sep="_") %>%
      mutate_at(c("N","k","alpha","T","lin","h","p"), function(x){
        x=gsub("[A-z]-","",x) 
        x=gsub("[a-z]","",x)
        x=gsub(".RDS","",x) 
      }) %>%
      mutate(mejora_total=penalizacion_inicial-penalizacion_final,
             mejora=1-(penalizacion_final/penalizacion_inicial),
             distancia_optimo= penalizacion_final/penalizacion_optima ) %>%
      summarise(t=first(T) %>% as.numeric(), 
                alpha=first(alpha),
                penalizacion_inicial=sum(penalizacion_inicial, na.rm = TRUE),
                penalizacion_final=sum(penalizacion_final, na.rm = TRUE),
                mejora_promedio=weighted.mean(mejora,total, na.rm = TRUE ),
                penalizacion_optima=sum(penalizacion_optima,na.rm = TRUE)) }) %>%
  filter(penalizacion_inicial== getmode(penalizacion_inicial) ) %>%
  mutate(penalizacion_observada= {caso_base %>%
      filter(id %in% seleccionadas) %>%
      summarise_all(sum) %>%
      .$penalizacion_final}) %>%
  mutate(mejora_total=penalizacion_inicial-penalizacion_final,
         # mejora=(penalizacion_final-penalizacion_observada)/penalizacion_observada ,
         mejora=1-(penalizacion_final/penalizacion_observada) ,
         distancia_optimo= penalizacion_final/penalizacion_observada ) %>%
  mutate(t=t/(60*60))%>%
  arrange(t) %>%
  select(mejora,t, alpha) %>%
  filter(!is.na(alpha)) %>%
  mutate(mejora=scales::percent(mejora)) %>%
  spread(alpha, mejora) %>%
  select(t,1,2,4,3) %>%
  xtable( type = "latex")  %>%
  print( file = "output/resultados_t.tex",label=NULL,include.rownames=FALSE)


# temp=readRDS(paste0("cache/acumulado_escenarios/",x))%>% 
#   mutate(mejora_total=penalizacion_inicial-penalizacion_final,mejora=1-(penalizacion_final/penalizacion_inicial))
# 

# nombre=separate(x %>% data.frame(name=.), name,into = c("n","k","alpha","t"),sep = "_")
# temp=readRDS(paste0("cache/resumen_escenarios/",x))%>% 
#   # filter(id %in% seleccionadas) %>%
#   select(estacion,penalizacion_final,penalizacion_inicial) %>%
#   summarise_all(sum) %>%
#   mutate(mejora_total=penalizacion_inicial-penalizacion_final,
#          mejora=1-(penalizacion_final/penalizacion_inicial), t=gsub("[^0-9]","",nombre$t) %>% as.numeric(),alpha=gsub("[^0-9]","",nombre$alpha)  %>% as.numeric())
# 
# }) %>%
# bind_rows({
#   caso_base %>%
#     filter(id %in% seleccionadas) %>%
#     summarise_all(sum) %>%
#     mutate(mejora_total=penalizacion_inicial-penalizacion_final,
#            mejora=(penalizacion_final-penalizacion_inicial) / penalizacion_inicial,
#            # distancia_optimo= penalizacion_final/penalizacion_optima ,
#            t=19800, alpha=1 %>% as.character())  }) %>%