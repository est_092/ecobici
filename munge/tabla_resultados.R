files[grepl("k-30", files)   & grepl("n-40",files) & (!grepl("linearizacion",files)) ]   %>%
  map_dfr(function(x){
    temp=readRDS(paste0("cache/acumulado_escenarios/",x)) %>% 
      mutate(mejora=1-(penalizacion_final/penalizacion_inicial))
    
  }) %>%
  mutate(t=t/(60*60),
         alpha=as.factor(round(alpha,digits = 4))) %>%
  select(alpha, mejora, t) %>%
  filter(!is.na(alpha)) %>%
  arrange(alpha,t) %>%
  mutate(mejora=scales::percent(mejora)) %>%
  spread(alpha, mejora) 
  


