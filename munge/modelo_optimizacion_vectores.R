library(dplyr)

##Función objetivo

f <- function(t=t_mat, alpha=alpha_1,n) {
  # f <- function(x) {
  # sum(x[(2*n^2 +(2*n)+1) :(2*n^2 +3*n)  ])  + ( alpha  *  sum(as.vector(t) )
   return (c( 
     ##T _ij
     alpha*as.vector(t(t)),
        rep(0, n^2),
        rep(0, n),
        rep(0, n),
        rep(0, n),
        rep(1, n),
        rep(0, n)))
}

##Primera restricción
##Inventory Balance    n
## = si0
r2=function(n){
  res=NULL
  for (n1 in 0:(n-1)){
    temp=rep(0,2*n^2+5*n)
    temp[2*n^2 +2*n +1 +n1]=1 ##si
    temp[2*n^2 +1 + n1]=+1    ##y_load
    temp[2*n^2 + n+1 +n1]=-1   ##y_unload
    res=res %>%
      rbind(temp)
  }
  return (res)
}
r2(40)
##Conservation of inventory in each vehicle  n    
## = 0
r3=function(n){
  res=NULL
  for (n1 in 0:(n-1)){
    temp=rep(0,2*n^2+5*n)
    temp[(n^2 +1 + n*n1): (n^2 +n + n*n1)]=-1 ##yij   Suma sobre j excepto i=j
    temp[(n^2 +1 + n*n1) + (n1) ]= 0
    
    
    temp[ seq(from=(n^2 +1 + n1),  to=(2* n^2 - (n-1)  +n1 ), by= n)]=1 ##yji   Suma sobre j excepto i=j
    temp[(n^2 +n1 +1 + n*n1)]= 0

    temp[2*n^2 + n1 +1 ]=1    ##y_load
    temp[2*n^2 + n+1 +n1]=-1   ##y_unload
    res=res %>%
      rbind(temp)
  }
  return (res)
}
##Vehicle capacity
##Se usa setdiff para i!=j    n*n - n
## <= 0
# x[setdiff((n^2 +1) :(2*n^2) , seq(from=(n^2 +1) , to=(2*n^2), by= (n+1) ))] - 
#   k * x[ setdiff(1:n^2 , seq(from=(1) , to=(n^2), by= (n+1) ))]

r4=function(n,k=k1){
  res=NULL
  for (n1 in 0: (n-1) ){
    for(n2 in 0: (n-1)){
      if(n1 != n2){
        temp=rep(0,2*n^2+5*n)
        temp[ n*n1  +1+ n2  ]=-k
        temp[ n^2 + n*n1  +1+ n2  ]=1
        res=res %>%
          rbind(temp)
      }
      
    }
  }
  # for (n1 in 0:(n^(2)-1)){
  #   
  #   
  #   temp=rep(0,2*n^2+5*n)
  #   if(!n1 %in% seq(from=(n^2 +1), to=(2*n^2), by= (n+1) )){
  #   temp[ n1 +1  ]=-k  ##Para cada xij  excepto i=j
  #   temp[(n^2 +n1 +1)]= 1 ##Para cada yij    excepto i=j
  #   res=res %>%
  #     rbind(temp)
  #   }
  # }
  return (res)
}
##Vehicle flow conservation  n
## = 0

r5=function(n){
  res=NULL
  for (n1 in 0: (n-1) ){
    temp=rep(0,2*n^2+5*n)
      temp[ (n*n1 +1) : (n *n1 + n )]=1  ##Suma sobre i de xij  excepto i=j
      temp[seq(n1 +1 ,n^2 , by=n)]=-1 ##Suma sobre j de xji  excepto i=j
      temp[n1*n + n1 +1]=0
      res=res %>%
        rbind(temp)
  }
  return (res)
}

##Each station is visited once at most by vehicle n
## <= 1
r6=function(n){
  res=NULL
  for (n1 in 1: (n-1) ){
    temp=rep(0,2*n^2+5*n)
    temp[ (n*n1 +1) : (n *n1 + n )]=1  ##Suma sobre i de xij  excepto i=j
    temp[n1*n + n1 +1]=0
    res=res %>%
      rbind(temp)
  }
  return (res)
}


## Quantity picked up at station n 
## <= si0
r7=function(n){
  res=NULL
  for (n1 in 0: (n-1) ){
    temp=rep(0,2*n^2+5*n)
    temp[ (2*n^2)+1+n1]=1  ##Suma sobre v de yi_U para cada i
    temp[n1*n + n1 +1]=0
    res=res %>%
      rbind(temp)
  }
  return (res)
}

## Quantity  unloaded at station    n
## <= ci - si0
r8=function(n){
  res=NULL
  for (n1 in 0: (n-1) ){
    temp=rep(0,2*n^2+5*n)
    temp[ ((2*n^2)+1+n1+n)]=1  ##Suma sobre v de yi_L para cada i
    temp[n1*n + n1 +1]=0
    res=res %>%
      rbind(temp)
  }
  return (res)
}


## All bicycles loaded are unloaded   1
## = 0 
r9=function(n){
  res=NULL
  # for (n1 in 0: (n-1) ){
    temp=rep(0,2*n^2+5*n)
    temp[( (2*n^2)+1):  ((2*n^2)+n)]=1  ##Suma sobre v de yi_U para cada i
    temp[ (((2*n^2)+1+n)): (((2*n^2)+2*n))]=-1  ##Suma sobre v de yi_L para cada i
    res=res %>%
      rbind(temp)
  # }
  return (res)
}


## Time Constraint  1
## <= T 
r10=function(n,L=15,U=15, t=t_mat){
    res=NULL
    temp=rep(0,2*n^2+5*n)
    temp[ ((2*n^2)+1):((2*n^2)+n)]=L  ##Suma sobre v de yi_U para cada i
    temp[ ((2*n^2)+1+n): ((2*n^2)+2*n)]=U  ##Suma sobre v de yi_L para cada i
    temp[1:n^2]=as.vector(t(t))
    temp[seq(1,n^2, by=(n+1))]=0
  return (temp)
}


##Sub - Tour Elimination n*n - (2n -1 )
## >= -1 +M 
r11=function(n,M=n){
  res=NULL
  for (n1 in 0: (n-1) ){
    for(n2 in 1: (n-1)){
      if(n1 != n2){
        temp=rep(0,2*n^2+5*n)
        temp[2*n^2 + 4*n +1 +n1]=-1
        temp[2*n^2 + 4*n +1 +n2]=+1
        temp[n1*n +n2 +1 ]=-M
        res=res %>%
          rbind(temp)
      }
   
    }
  }
  return (res)
}
load("data/linearizacion.RData")
linear=linearizacion%>%
  mutate(id=estacion) 
r12=function(n,linearizacion=linear){
  res=NULL
  # i=0
  for (n1 in 2: n ){
    ci=linearizacion %>%
      filter(id==n1)%>%
      summarise(n=max(i0)) %>%.$n
    
    for(u in 0: (ci -1) ){
      temp=rep(0,2*n^2+5*n)
      temp[2*n^2 +2*n  +n1]= linearizacion %>%                ##s_i
        filter(id==n1 & i0==u)%>% .$b_i  *(-1)
      temp[2*n^2 +3*n +n1]=1   ##g_i
      res=res %>%
        rbind(temp)
      # i=i+1
    }
    
    
  }
  return (res)
}
rhs_r12=function(n,linearizacion=linear){
  res=NULL
  # i=0
  for (n1 in 2: n ){
    ci=linearizacion %>%
      filter(id==n1)%>%
      summarise(n=max(i0)) %>%.$n
    
    for(u in 0: (ci -1) ){
      temp= linearizacion %>%                ##s_i
        filter(id==n1 & i0==u)%>% .$a_i  
      res=c(res,temp)
      # i=i+1
    }
    
    
  }
  return (res)
}

n=10
alpha_1=0
v=1


t_mat=matrix(1,nrow=n,ncol = n)
x=rep(0, 2*n^2+5*n)
k1=20
L=U=10
M=n
T=60*60*5
test=matrix_dist %>%
  select(x=id.x,y=id.y, time=Time) %>%
  filter(x<=n & y<=n) %>%
  arrange(x,y) %>%
  spread(y,time) %>%
  select(-x) %>%
  as.matrix()
# s0=1:40
# s0=rep(40,n)
load("cache/capacity.RData")
c=capacity %>% filter(id<=n) %>% .$capacity 
# c[1]=1000
s0=floor(c/2)

g= r2(n) %>%
  rbind(r3(n)) %>%
  rbind(r4(n)) %>%
  rbind(r5(n)) %>%
  rbind(r6(n)) %>%
  rbind(r7(n)) %>%
  rbind(r8(n)) %>%
  rbind(r9(n)) %>%
  rbind(r10(n,t=test)) %>%
  rbind(r11(n)) %>%
  rbind(r12(n,linearizacion = linear))




prueba=r4(n)
# %>% t() %>% as.matrix()
colnames(prueba)=names
prueba


n_r12=length(r12(n,linearizacion = linear)[,1])

f1=f(n=n,t=test)

milp <- OP(L_objective(f1, 
                       # n=(2*n^2+5*n) ,
                       names =  c(paste0("x",1:n^2),
                                  paste0("y",1:n^2) ,
                                  paste0("y_L",1:n),
                                  paste0("y_U",1:n),
                                  paste0("s",1:n),
                                  paste0("g",1:n),
                                  paste0("q",1:n)
                       )
                       ), 
           L_constraint(g, dir = c( rep("==", n),
                                    rep("==", n),
                                    rep("<=", (n*n - n)),
                                    rep("==", n),
                                    rep("<=", (n-1) ),
                                    rep("<=", n),
                                    rep("<=", n),
                                    rep("==", 1),
                                    rep("<=", 1),
                                    rep(">=", (n*n - (2*n -1 ))),
                                    rep(">=", n_r12))
                        , rhs = c(s0,
                                  rep(0, n),
                                  rep(0, (n*n - n)),
                                  rep(0, n),
                                  rep(1, (n-1)),
                                  s0,
                                  c-s0,
                                  rep(0,1),
                                  rep(T, 1),
                                  rep(-M+1, (n*n - (2*n -1 ))),
                                  rhs_r12(n,linearizacion = linear))),
           types = c( rep("B", n^2),
                      rep("C", n^2),
                      rep("I", n),
                      rep("I", n),
                      rep("C", n),
                      rep("C", n),
                      rep("C", n)),
           maximum = FALSE)
sol$objval
(sol <- ROI_solve(milp))
solution(sol)[solution(sol) >0]



linear %>%
  .$estacion%>%
  unique() %>%
  map (function(x){
    jpeg(paste0("output/plot/",x,".jpg"), width = 350, height = 350)
    linear %>%
    filter(estacion==x) %>%
      mutate(y=a_i+ b_i*i0) %>%
      select(i0, y) %>%plot
    dev.off()
    
  })
  


estaciones%>%
  # filter(id<=n) %>%
  mutate(coordenadas=paste0(`location.lat`,",",`location.lon`)) %>%
  select(id, coordenadas)

resumen=estaciones%>%
  mutate(coordenadas=paste0(`location.lat`,",",`location.lon`)) %>%
  select(id, name, coordenadas) %>%
  left_join({capacity},by=c("id")) %>%
  left_join({
    linear %>%
      # filter(estacion==x) %>%
      mutate(y=a_i+ b_i*i0) %>%
      group_by(estacion) %>%
      mutate(min=min(y)) %>%
      ungroup() %>%
      filter(y==min) %>%
      select( estacion,inv_optimo=i0)  }, by=c("id"="estacion")) %>%
  left_join({
    sol$solution %>% as.data.frame() %>% tibble::rownames_to_column("ID") %>% 
      filter(grepl("s",ID)) %>% mutate(ID=gsub("s","",ID) %>% as.numeric()) %>% dplyr::select(id=ID, inv_final=2) }, by=c("id"))
  
  
  
  linear %>%
      # filter(estacion==x) %>%
      mutate(y=a_i+ b_i*i0) %>%
      group_by(estacion) %>%
      mutate(min=min(y)) %>%
      ungroup() %>%
      filter(y==min) %>%
      filter(estacion<=n) %>%
      mutate(s0=s0) %>%
      select(estacion,id, min, inv_optimo=i0, s0) %>%
      left_join({
        sol$solution %>% as.data.frame() %>% tibble::rownames_to_column("ID") %>% 
          filter(grepl("s",ID)) %>% mutate(ID=gsub("s","",ID) %>% as.numeric()) %>% dplyr::select(id=ID, inv_final=2) }, by=c("id"))
      
   


ROI_applicable_solvers(milp)
ROI_available_solvers(milp)

