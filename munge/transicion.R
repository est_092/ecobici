horas=c(0:23)
minutos=c(00,15,30,45,60)
etiquetas=c("00:00-00:15","00:15-00:30","00:30-00:45","00:45-01:00",
            "01:00-01:15","01:15-01:30","01:30-01:45","01:45-02:00",
            "02:00-02:15","02:15-02:30","02:30-02:45","02:45-03:00",
            "03:00-03:15","03:15-03:30","03:30-03:45","03:45-04:00",
            "04:00-04:15","04:15-04:30","04:30-04:45","04:45-05:00",
            "05:00-05:15","05:15-05:30","05:30-05:45","05:45-06:00",
            "06:00-06:15","06:15-06:30","06:30-06:45","06:45-07:00",
            "07:00-07:15","07:15-07:30","07:30-07:45","07:45-08:00",
            "08:00-08:15","08:15-08:30","08:30-08:45","08:45-09:00",
            "09:00-09:15","09:15-09:30","09:30-09:45","09:45-10:00",
            "10:00-10:15","10:15-10:30","10:30-10:45","10:45-11:00",
            "11:00-11:15","11:15-11:30","11:30-11:45","11:45-12:00",
            "12:00-12:15","12:15-12:30","12:30-12:45","12:45-13:00",
            "13:00-13:15","13:15-13:30","13:30-13:45","13:45-14:00",
            "14:00-14:15","14:15-14:30","14:30-14:45","14:45-15:00",
            "15:00-15:15","15:15-00:30","15:30-15:45","15:45-16:00",
            "16:00-16:15","16:15-16:30","16:30-16:45","16:45-17:00",
            "17:00-17:15","17:15-17:30","17:30-17:45","17:45-18:00",
            "18:00-18:15","18:15-18:30","18:30-18:45","18:45-19:00",
            "19:00-19:15","19:15-19:30","19:30-19:45","19:45-20:00",
            "20:00-20:15","20:15-20:30","20:30-20:45","20:45-21:00",
            "21:00-21:15","21:15-21:30","21:30-21:45","21:45-22:00",
            "22:00-22:15","22:15-22:30","22:30-22:45","22:45-23:00",
            "23:00-23:15","23:15-23:30","23:30-23:45","23:45-00:00")
##Llegadas
for (estacion in Polanco)
{
    
    R1=which(DatosPol$Ciclo_Estacion_Arribo==estacion )
    DatosHist=DatosPol[R1,]
    ocurrencias=matrix(0,nrow = 96, ncol = 31)
    
    for (day in 1:31)
    {
      R2=which(days(DatosHist$Hora_Arribo)==day)
      DatosHist2=DatosHist[R2,]
      contador=1
      for ( hora in horas)
      {
        for(periodo in 1:4)
        {  
     
        llegadas=which(hours(DatosHist2$Hora_Arribo)==hora & minutes(DatosHist2$Hora_Arribo)>=minutos[periodo] 
                       & minutes(DatosHist2$Hora_Arribo)<=minutos[periodo+1])
        ocurrencias[contador,day]=length(llegadas)
        contador=contador +1
        }
      }
    } 
    rownames(ocurrencias)=etiquetas
    name=paste("llegadas_",estacion,sep="")
    name=paste(name,".csv",sep="")
    write.csv(ocurrencias,name)
}

##Salidas
for (estacion in Polanco)
{
  
  R1=which(DatosPol$Ciclo_Estacion_Retiro==estacion )
  DatosHist=DatosPol[R1,]
  ocurrencias=matrix(0,nrow = 96, ncol = 31)
  
  for (day in 1:31)
  {
    R2=which(days(DatosHist$Hora_Retiro)==day)
    DatosHist2=DatosHist[R2,]
    contador=1
    for ( hora in horas)
    {
      for(periodo in 1:4)
      {  
        
        salidas=which(hours(DatosHist2$Hora_Retiro)==hora & minutes(DatosHist2$Hora_Retiro)>=minutos[periodo] 
                       & minutes(DatosHist2$Hora_Retiro)<=minutos[periodo+1])
        ocurrencias[contador,day]=length(salidas)
        contador=contador +1
      }
    }
  } 
  rownames(ocurrencias)=etiquetas
  name=paste("salidas_",estacion,sep="")
  name=paste(name,".csv",sep="")
  write.csv(ocurrencias,name)
}

##Demanda neta del per�odo
for (estacion in Polanco)
{
  
  R1_a=which(DatosPol$Ciclo_Estacion_Arribo==estacion )
  R1_r=which(DatosPol$Ciclo_Estacion_Retiro==estacion )
  DatosHist_a=DatosPol[R1_a,]
  DatosHist_r=DatosPol[R1_r,]
  arribos=matrix(0,nrow = 96, ncol = 31)
  retiros=matrix(0,nrow = 96, ncol = 31)
  for (day in 1:31)
  {
    R2_a=which(days(DatosHist_a$Hora_Arribo)==day)
    R2_r=which(days(DatosHist_r$Hora_Retiro)==day)
    DatosHist2_a=DatosHist_a[R2_a,]
    DatosHist2_r=DatosHist_r[R2_r,]
    contador=1
    for ( hora in horas)
    {
      for(periodo in 1:4)
      {  
        
        llegadas=which(hours(DatosHist2_a$Hora_Arribo)==hora & minutes(DatosHist2_a$Hora_Arribo)>=minutos[periodo] 
                       & minutes(DatosHist2_a$Hora_Arribo)<=minutos[periodo+1])
        arribos[contador,day]=length(llegadas)
        salidas=which(hours(DatosHist2_r$Hora_Retiro)==hora & minutes(DatosHist2_r$Hora_Retiro)>=minutos[periodo] 
                      & minutes(DatosHist2_r$Hora_Retiro)<=minutos[periodo+1])
        retiros[contador,day]=length(salidas)
        contador=contador +1
      }
    }
  } 
 
  rownames(arribos)=etiquetas
  rownames(retiros)=etiquetas
  demandas=arribos-retiros
  name=paste("demanda_",estacion,sep="")
  name=paste(name,".csv",sep="")
  write.csv(demandas,name)
}


