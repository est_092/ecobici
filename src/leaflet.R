library(leaflet)

lat_centro=19.4319716
long_centro=-99.1356141



pal <- colorNumeric(
  # palette = "PiYG",
  palette = c("green","red"),
  domain = resumen$estado_final)

icons <- awesomeIcons(
  icon = 'fa-bicycle',
  iconColor =   ~pal(estado_final),
  library = 'fa')
  # markerColor = "white"


estacion_popup <- paste0("<strong>Estacion: </strong>",
                         resumen$estacion,
                         "<br><strong>Nombre: </strong>",
                         resumen$name,
                         "<br><strong>Capacidad:</strong>",
                         resumen$capacity%>% round(),
                         "<br><strong>Inventario Inicial:</strong>",
                         resumen$s0%>% round(),
                         "<br><strong>Inventario Óptimo:</strong>",
                         resumen$inv_optimo%>% round(),
                         "<br><strong>Inventario Final:</strong>",
                         resumen$inv_final%>% round(),
                         "<br><strong>Estado Inicial:</strong>",
                         resumen$estado_inicial%>% round(digits=2),
                         "<br><strong>Estado Final:</strong>",
                         resumen$estado_final%>% round(digits=2))
label= paste0("Estacion:",
              resumen$estacion)
# options = leafletOptions(minZoom = 12, dragging = TRUE)
leaflet() %>%
  # addTiles() %>%
  addProviderTiles(provider="Esri") %>%
  setView(lat = lat_centro, lng = long_centro, zoom = 12) %>%
  setMaxBounds(lng1 = long_centro + .15, 
               lat1 = lat_centro + .15, 
               lng2 = long_centro - .15, 
               lat2 = lat_centro - .15) %>%
  # addCircleMarkers(data=resumen, lng = ~longitud,lat=~latitud ,popup = estacion_popup, radius = ~ capacity, color = ~pal(cambio),
  #                  labelOptions = labelOptions(noHide = T, textOnly = TRUE))
  addAwesomeMarkers(data=resumen, lng = ~longitud,lat=~latitud ,popup = estacion_popup, icon=icons,
             # label = label,
             labelOptions = labelOptions(noHide = T, textOnly = TRUE) )
# %>%
#   addPolylines(data = poly_viajes, lng = ~lon, lat = ~lat, group = ~id) 


  label <- paste(sep = "<br/>",'long label1', 'long label2', 'long label3','long label4', 'long label5', 'long label6')
  
  markers <- data.frame(lat,long,label)
  

estaciones=read.csv("data/estaciones.csv")
viajes=sol$solution %>% as.data.frame() %>% tibble::rownames_to_column("ID") %>% 
  rename(value=2) %>%
  filter(grepl("x",ID)) %>% mutate(ID=gsub("x","",ID) %>% as.numeric()) %>% 
  mutate( origin= ceiling( ID/n) ,destiny= ifelse(ID%%n==0,n,ID%%n)) %>%
  filter(value==1) %>%
  select(origin,destiny) %>%
  left_join(data_estaciones %>% rename(coordenadas_origin=coordenadas), by=c("origin"="id")) %>%
  left_join(data_estaciones %>% rename(coordenadas_destiny=coordenadas), by=c("destiny"="id"))

data_estaciones=estaciones%>%
  # filter(id<=n) %>%
  mutate(coordenadas=paste0(`location.lat`,",",`location.lon`)) %>%
  select(id, coordenadas)
