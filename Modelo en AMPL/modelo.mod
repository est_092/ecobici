set N;
set N_0= N union {0};
param estaciones {N};
set V;
set Us;
param s_0 {N_0} >=0;
param c {N_0}; ##checar 
param k {V};
param f {i in N, 0 .. c[i]};
param t { N_0,  N_0};
param weight;
param T;
param L;
param U;
param M; ##checar
param a {N, Us};
param b {N, Us};

var x {i in N_0, j in N_0,V} binary;
var y {i in N_0, j in N_0,V} >=0;
var y_L {N_0,V} integer >=0;
var y_U {N_0,V} integer >=0;
var q {N_0,V} >=0;
var s {N_0} integer >=0;
var g {N}  >=0;

minimize Respositioning_Cost: sum{i in N}( g[i]) +weight* (sum{i in N_0} sum{j in N_0} sum{v in V} t[i,j]*x[i,j,v]);
subject to Capacity { i in N_0}:
s[i]=s_0[i]- sum{v in V}(y_L[i,v]-y_U[i,v]);
subject to Conservation_Inventory {i in N_0, v in V}:
y_L[i,v]-y_U[i,v]= sum{j in N_0: j<>i}(y[i,j,v])-sum{j in N_0: j!=i}(y[j,i,v]);
subject to Vehicle_Capacity {i in N_0, j in N_0, v in V : j<>i }:
y[i,j,v]<=k[v] * x[i,j,v];
subject to Vehicle_Flow {i in N_0, v in V}:
sum{j in N_0: j<>i}x[i,j,v]=sum{j in N_0: j<>i}x[j,i,v];
subject to Visit_Once { i in N, v in V}:
sum{j in N_0: j<>i} x[i,j,v]<= 1;
subject to Limit_Load {i in N_0}:
sum{v in V} y_L [i,v]<=s_0[i];
subject to Limit_Unload {i in N_0}:
sum{v in V} y_U [i,v]<=c[i] - s_0[i];
subject to All_Loaded_Unloaded {v in V}:
sum{ i in N_0}(y_L[i,v] - y_U[i,v])=0;
subject to Time { v in V}:
sum{ i in N}(L*y_L[i,v]+U*y_U[i,v])+sum{ i in N}(L*y[0,i,v]+U*y[i,0,v])+ sum{i in N_0, j in N_0: j<>i} (t[i,j]*x[i,j,v])<= T;
subject to Subtour_Elimination {i in N_0,j in N, v in V : i<>j}:
q[j,v]>= q[i,v] +1 - M*(1-x[i,j,v]);
##Nuevas
subject to Linearization {i in N, u in Us: (c[i]-1)>=u}:
g[i]>= a[i,u]+b[i,u]*s[i];    ##Revisar indices de u 
subject to Vehicle_Departs {v in V}:
sum{j in N} x[0,j,v] >=1;
subject to Loading_Limit {i in N, v in V}:
y_L[i,v]<= min(s_0[i],k[v])*sum{j in N_0}(x[i,j,v]);
subject to Unloading_Limit {i in N, v in V}:
y_U[i,v]<=min((c[i]-s_0[i]),k[v])*sum{j in N_0}(x[i,j,v]);
