library(openxlsx)
library(htmlwidgets)
library(dplyr)
library(leaflet)
# library(rJava)
# setwd("~/Ecobici/R")

diccionario=read.xlsx("./data/resultados_1_vehiculo_05_07.xlsx",sheet='diccionario')
x=read.xlsx("./data/resultados_1_vehiculo_05_07.xlsx",sheet='x') %>% select(estacion.inicio=Estacion.Inicio,estacion.fin=Estacion.Fin,valor=Valor)

y=read.xlsx("./data/resultados_1_vehiculo_05_07.xlsx",sheet='y') %>% 
  select(inicio=Indice,fin=X3,valor=Valor) %>% left_join(diccionario,by=c("inicio"="numero")) %>% 
  left_join(diccionario,by=c("fin"="numero")) %>%
  select(estacion.inicio=estacion.x,estacion.fin=estacion.y,valor)
s=read.xlsx("./data/resultados_1_vehiculo_05_07.xlsx",sheet='s') %>% 
  select(inicio=Indice,valor=Valor) %>% left_join(diccionario,by=c("inicio"="numero")) %>%
  select(estacion.inicio=estacion,valor)
c_0=read.table("./data/estado_01am.csv",header = TRUE)
c_0=c_0[which(c_0$id %in% Polanco),] %>% select( estacion=id, c_0=availability__bikes)


capacidad=read.csv("./data/disponibilidad.csv") %>% 
   left_join(diccionario,by=c("id"="numero")) 
capacidad$capacidad=capacidad$availability.bikes+capacidad$availability.slots
capacidad=capacidad %>% select(estacion,capacidad)
base=x[which(x$valor==1 & x$estacion.inicio!=x$estacion.fin),] %>% left_join(y,by=c("estacion.inicio","estacion.fin"))%>% 
  left_join(capacidad,by=c("estacion.inicio"="estacion"))%>% 
  left_join(s,by=c("estacion.inicio")) %>% select(estacion.inicio,estacion.fin,bicicletas_enviadas=valor.y,inventario_final=valor,capacidad)
estaciones=read.csv("./data/estaciones.csv")
estaciones=estaciones[,c(1,10,11)]
colnames(estaciones)=c("estacion","latitud","longitud")
base_inversa=base [,c("estacion.fin","bicicletas_enviadas")]
 
base=base %>% left_join(estaciones,by=c("estacion.inicio"="estacion")) %>%
  left_join(base_inversa,by=c("estacion.inicio"="estacion.fin")) %>%
  left_join(c_0,by=c("estacion.inicio"="estacion")) %>%
  select(estacion=estacion.inicio, destino=estacion.fin, bicicletas_enviadas=bicicletas_enviadas.x, 
         bicicletas_recibidas=bicicletas_enviadas.y,inventario_final,longitud,latitud,capacidad,c_0) 
#base=merge(x,estaciones)
base$cambio=(base$inventario_final-base$c_0)/base$c_0

mymap=leaflet(data = base) %>%
  addTiles(group = "OSM") %>%
  addProviderTiles("Stamen.TonerLite") %>%
  addLayersControl(
    baseGroups = c("OSM", "Stamen.TonerLite"))
estacion_popup <- paste0("<strong>Estacion: </strong>",
                         base$estacion,
                         "<br><strong>Destino: </strong>",
                         base$destino,
                         "<br><strong>Bicicletas Enviadas:</strong>",
                         base$bicicletas_enviadas,
                         "<br><strong>Bicicletas Recibidas:</strong>",
                         base$bicicletas_recibidas,
                         "<br><strong>Capacidad:</strong>",
                         base$capacidad,
                         "<br><strong>Inventario Final:</strong>",
                         base$inventario_final)
label= paste0("Estacion:",
              base$estacion)
pal <- colorQuantile("YlGn", NULL, n = 5)

a=pal(DF$pobtot)
a.t=table(a)
a.t.at=attributes(a.t)
colLegend <- rev(a.t.at$dimnames$a)

mymap=addAwesomeMarkers(mymap, lng =~longitud, lat = ~latitud,popup = estacion_popup, colors=colLegend, label = label, labelOptions = labelOptions(noHide = T, textOnly = TRUE))


saveWidget(mymap,"mapa.html")
